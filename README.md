# test_the_mermaid

Testing long links

```mermaid
graph TD
    A[Start] --> B{Is it?};
    B -->|Yes| C[OK];
    C --> D[Rethink];
    D --> B;
    B ---->|No| E[End];
```

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbiAgICBBW1N0YXJ0XSAtLT4gQntJcyBpdD99O1xuICAgIEIgLS0-fFllc3wgQ1tPS107XG4gICAgQyAtLT4gRFtSZXRoaW5rXTtcbiAgICBEIC0tPiBCO1xuICAgIEIgLS0tLT58Tm98IEVbRW5kXTsiLCJtZXJtYWlkIjp7InRoZW1lIjoiYmFzZSJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbiAgICBBW1N0YXJ0XSAtLT4gQntJcyBpdD99O1xuICAgIEIgLS0-fFllc3wgQ1tPS107XG4gICAgQyAtLT4gRFtSZXRoaW5rXTtcbiAgICBEIC0tPiBCO1xuICAgIEIgLS0tLT58Tm98IEVbRW5kXTsiLCJtZXJtYWlkIjp7InRoZW1lIjoiYmFzZSJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)
